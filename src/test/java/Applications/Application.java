package Applications;

public interface Application {

    void forceStop();

    void clearData();

    Object open();


    String packageID();

    String activityID();

    Integer version ();



}
