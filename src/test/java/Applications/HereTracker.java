package Applications;

import io.appium.java_client.AppiumDriver;
import pageObjects.screens.AddDeviceScreen;
import pageObjects.screens.CredentialsScreen;
import pageObjects.screens.TermsScreen;
import utilities.ADB;

/**
 * Created by vladyslav on 1/11/18.
 */
public class HereTracker implements Application {

    AppiumDriver driver;
    ADB adb;



    public HereTracker (AppiumDriver driver){
        this.driver=driver;
    }

    public TermsScreen termsAndConditions (){
        return new TermsScreen(driver);
    }

    public AddDeviceScreen addDeviceScreen (){
        return new AddDeviceScreen(driver);
    }

    public CredentialsScreen deviceCredentials(){
        return new CredentialsScreen(driver);
    }


    public void forceStop() {
        adb.forceStopApp(packageID());
    }

    public void clearData() {
        adb.clearAppsData(packageID());

    }

    public Object open() {

      adb.startApp(packageID(), activityID());
        return null;
    }

    public String packageID() {
        return "com.monese.monese.dev";
    }

    public String activityID() {
        return "com.monese.monese.activities.LauncherActivity";
    }

    public Integer version() {
        return null;
    }



}
