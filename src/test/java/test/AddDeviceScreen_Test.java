package test;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by vladyslav on 1/12/18.
 */
public class AddDeviceScreen_Test extends BaseTest {

    @Test(priority = 1, groups = {"app"})
    public void test_addDeviceScreenCorrectDisplaying (){
        trackerApp.termsAndConditions()
                .nextBtn.click();
        Assert.assertTrue(trackerApp.addDeviceScreen()
                .scanBtn.isDisplayed());
        Assert.assertTrue(trackerApp.addDeviceScreen().scanBtn.isEnabled());
    }
}
