package test; /**
 * Created by vladyslav on 1/8/18.
 */

import Applications.HereTracker;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import lombok.Getter;
import lombok.Setter;
import org.testng.annotations.*;

import java.lang.reflect.Method;

import utilities.AppiumSession;
import utilities.Capabilities;



public class BaseTest {

    AppiumSession appiumSession;

   @Setter @Getter
   AppiumDriver driver;


    Capabilities capabilities;
    HereTracker trackerApp;
    String appType;





public AppiumDriver appiumDriver (AppiumServiceBuilder builder, Capabilities  capabilities){
   return new AppiumDriver(appiumSession.getBuilder(), capabilities);

}

    @Parameters  ({"deviceName"})
@BeforeMethod (alwaysRun = true)
public void setUp(@Optional("default") String deviceName, Method m) throws Exception {


    Test t = m.getAnnotation(Test.class);
    t.groups();
    appType=t.groups()[0];

    capabilities = new Capabilities();

    //capabilities = new Capabilities(deviceName,appType);

    appiumSession = new AppiumSession();
   // appiumSession.start();
   // setDriver(appiumDriver(
   //        appiumSession.getBuilder(), capabilities)
   // );
        driver = new AppiumDriver(appiumSession.getBuilder(), capabilities.getCap(deviceName,appType));
    //driver = appiumSession.driver();
    trackerApp=new HereTracker(getDriver());
    if (appType.contains("browser") ) {
        driver.get("http://wego.here.com");
    }


}





@AfterMethod (alwaysRun = true)
    public void tearDown (){
    driver.quit();
}


}
