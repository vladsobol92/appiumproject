package test;

import org.testng.Assert;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Created by vladyslav on 1/11/18.
 */
public class TermsScreen_Test extends BaseTest {




    @Test(priority = 1, groups = {"app"})
    public void test_conditionsScreenCorrectDisplaying (){

        Assert.assertTrue(trackerApp.
                termsAndConditions()
                .title.isDisplayed());

        Assert.assertTrue(trackerApp.
                termsAndConditions()
                .serviceLink
                .getAttribute("clickable").equals("true"));

        Assert.assertTrue(trackerApp.
                termsAndConditions()
                .nextBtn
                .isEnabled());

    }


    @Test(priority = 1, groups = {"browser"})
    public void test_conditionsScreenCorrectDisplaying_browser (){

        Assert.assertTrue(
                trackerApp.termsAndConditions().title.isDisplayed()
        );

    }

}
