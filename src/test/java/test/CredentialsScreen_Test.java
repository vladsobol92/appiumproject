package test;

import org.testng.Assert;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;

/**
 * Created by vladyslav on 1/12/18.
 */
public class CredentialsScreen_Test extends BaseTest {






    @Test(priority = 1, groups = {"app"})
    public void test_doneButtonFunctionality() {

        trackerApp.termsAndConditions()
                .proceedToApp()
                .typeCredentialsBtn.click();

        Assert.assertFalse(trackerApp.deviceCredentials()
                .doneBtn.isEnabled());

        trackerApp.deviceCredentials()
                .setDeviceId("ae128b00-7cde-4df3-becb-50497f65ac01")
                .setDevicePassword("26cmDTzlxKnWAWRhLFxn7PEDBmWO-94X6PL_VCs9Ik4");

        Assert.assertTrue(trackerApp.deviceCredentials()
                .doneBtn.isEnabled());
    }
}
