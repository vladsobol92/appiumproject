package utilities;


import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.FileReader;

/**
 * Created by vladyslav on 1/11/18.
 */
public class Capabilities extends DesiredCapabilities {



    public Capabilities (String deviceName) throws Exception{

        setCapabilities(deviceName);

    }


    public Capabilities () throws Exception{

    }

    public void setCapabilities (String deviceName) throws Exception{



            JSONParser parser = new JSONParser();



            Object obj = parser.parse(new FileReader("/Users/vladyslav/IdeaProjects/apiumProject/capabilities.json"));



            JSONObject fileData = (JSONObject) obj;

            JSONObject androidCapabilities = (JSONObject) fileData.get("android");

            //String appActivity = (String) androidCapabilities.get("appActivity");
            setCapability("appActivity", (String) androidCapabilities.get("appActivity"));

            // String appWaitActivity = (String) androidCapabilities.get("appWaitActivity");
            setCapability("appWaitActivity", (String) androidCapabilities.get("appWaitActivity"));

            //Boolean  autoAcceptAlerts = (Boolean) androidCapabilities.get("autoAcceptAlerts");
            setCapability("autoAcceptAlerts", (Boolean) androidCapabilities.get("autoAcceptAlerts"));

            //  Boolean  autoGrantPermissions = (Boolean) androidCapabilities.get("autoAcceptAlerts");
            setCapability("autoGrantPermissions", (Boolean) androidCapabilities.get("autoGrantPermissions"));

            setCapability("app", (String) androidCapabilities.get("app"));
            setCapability("fullReset",androidCapabilities.get("fullReset"));



            JSONObject deviceCapabilities =  (JSONObject) fileData.get(deviceName);

          //  String platformVersion = (String)androidCapabilities.get("platformVersion");
            setCapability("platformVersion",(String)deviceCapabilities.get("platformVersion"));


          //  String deviceName = (String) androidCapabilities.get("deviceName");
            setCapability("deviceName",(String) deviceCapabilities.get("deviceName"));



            //String platformName = (String)androidCapabilities.get("platformName");
            setCapability("platformName", "Android");

            //setCapability("platformName", (String)androidCapabilities.get("platformName"));




            setCapability("newCommandTimeout", 60 * 2);




        /*
        catch (Exception e){
            e.printStackTrace();
        }
        */
    }



    public DesiredCapabilities getCap (String deviceName, String appType)throws Exception{

        DesiredCapabilities capabilities = new DesiredCapabilities();

        JSONParser parser = new JSONParser();



        Object obj = parser.parse(new FileReader("/Users/vladyslav/IdeaProjects/appiumproject/capabilities.json"));

        JSONObject fileData = (JSONObject) obj;


    if (appType.contains("browser")) {

        JSONObject browserCapabilities = (JSONObject) fileData.get("browser");
        capabilities.setCapability("browserName", (String) browserCapabilities.get("browserName"));

        capabilities.setCapability("autoAcceptAlerts", (Boolean) browserCapabilities.get("autoAcceptAlerts"));

        capabilities.setCapability("autoGrantPermissions", (Boolean) browserCapabilities.get("autoGrantPermissions"));
    }

    else {


        JSONObject androidCapabilities = (JSONObject) fileData.get("android");

        //String appActivity = (String) androidCapabilities.get("appActivity");
        capabilities.setCapability("appActivity", (String) androidCapabilities.get("appActivity"));

        // String appWaitActivity = (String) androidCapabilities.get("appWaitActivity");
        capabilities.setCapability("appWaitActivity", (String) androidCapabilities.get("appWaitActivity"));

        //Boolean  autoAcceptAlerts = (Boolean) androidCapabilities.get("autoAcceptAlerts");
        capabilities.setCapability("autoAcceptAlerts", (Boolean) androidCapabilities.get("autoAcceptAlerts"));

        //  Boolean  autoGrantPermissions = (Boolean) androidCapabilities.get("autoAcceptAlerts");
        capabilities.setCapability("autoGrantPermissions", (Boolean) androidCapabilities.get("autoGrantPermissions"));

        capabilities.setCapability("app", (String) androidCapabilities.get("app"));
        capabilities.setCapability("fullReset",androidCapabilities.get("fullReset"));

        }



        JSONObject deviceCapabilities =  (JSONObject) fileData.get(deviceName);

        //  String platformVersion = (String)androidCapabilities.get("platformVersion");
        capabilities.setCapability("platformVersion",(String)deviceCapabilities.get("platformVersion"));


        //  String deviceName = (String) androidCapabilities.get("deviceName");
        capabilities.setCapability("deviceName",(String) deviceCapabilities.get("deviceName"));



        //String platformName = (String)androidCapabilities.get("platformName");
        capabilities.setCapability("platformName", "Android");

        //setCapability("platformName", (String)androidCapabilities.get("platformName"));




        capabilities.setCapability("newCommandTimeout", 60 * 2);

        return capabilities;
    }


}
