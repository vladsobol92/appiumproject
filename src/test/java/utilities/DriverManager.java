package utilities;

import io.appium.java_client.AppiumDriver;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;

import java.io.FileReader;
import java.net.MalformedURLException;
import java.util.ArrayList;

public class DriverManager {

  static  AppiumDriver driver;

    private static String appPackage="com.monese.monese.dev";




    public static DesiredCapabilities getCap (String deviceName, String appType)throws Exception{

        ADB adb = new ADB(deviceName);

        DesiredCapabilities capabilities = new DesiredCapabilities();

        JSONParser parser = new JSONParser();



        Object obj = parser.parse(new FileReader("/Users/vladyslav/IdeaProjects/appiumproject/capabilities.json"));

        JSONObject fileData = (JSONObject) obj;


        if (appType.contains("browser")) {

            JSONObject browserCapabilities = (JSONObject) fileData.get("browser");
            capabilities.setCapability("browserName", (String) browserCapabilities.get("browserName"));

            capabilities.setCapability("autoAcceptAlerts", (Boolean) browserCapabilities.get("autoAcceptAlerts"));

            capabilities.setCapability("autoGrantPermissions", (Boolean) browserCapabilities.get("autoGrantPermissions"));
        }

        else {


            JSONObject androidCapabilities = (JSONObject) fileData.get("android");

            //String appActivity = (String) androidCapabilities.get("appActivity");
            capabilities.setCapability("appActivity", (String) androidCapabilities.get("appActivity"));

            // String appWaitActivity = (String) androidCapabilities.get("appWaitActivity");
            capabilities.setCapability("appWaitActivity", (String) androidCapabilities.get("appWaitActivity"));

            //Boolean  autoAcceptAlerts = (Boolean) androidCapabilities.get("autoAcceptAlerts");
            capabilities.setCapability("autoAcceptAlerts", (Boolean) androidCapabilities.get("autoAcceptAlerts"));

            //  Boolean  autoGrantPermissions = (Boolean) androidCapabilities.get("autoAcceptAlerts");
            capabilities.setCapability("autoGrantPermissions", (Boolean) androidCapabilities.get("autoGrantPermissions"));

            capabilities.setCapability("app", (String) androidCapabilities.get("app"));
            capabilities.setCapability("fullReset",androidCapabilities.get("fullReset"));

        }



        JSONObject deviceCapabilities =  (JSONObject) fileData.get(deviceName);

        //  String platformVersion = (String)androidCapabilities.get("platformVersion");
        capabilities.setCapability("platformVersion",adb.getAndroidVersionAsString());


        //  String deviceName = (String) androidCapabilities.get("deviceName");
        capabilities.setCapability("deviceName",deviceName);



        //String platformName = (String)androidCapabilities.get("platformName");
        capabilities.setCapability("platformName", "Android");

        //setCapability("platformName", (String)androidCapabilities.get("platformName"));




        capabilities.setCapability("newCommandTimeout", 60 * 2);

        return capabilities;
    }


/*
In this method we check devices available to run tests.
1. Create list of all connected devices
2. For each device check if the Application is installed
3. If Application is NOT installed we add this device to available devices ArrayList
4. If Application IS installed we DO NOT add this device to available devices ArrayList
5. If Application IS installed on ALL connected devices we throw RunTimeException
 */
    private static ArrayList <String> getAvailableDevices(){

        ArrayList<String> availableDevices=new ArrayList<String>();
        ArrayList connectedDevices=ADB.getConnectedDeviceName();

        for (Object connectedDevice:connectedDevices){

            String device=connectedDevice.toString();
            ArrayList apps=new ADB(device).getInstalledPackages();
            if(!apps.contains(appPackage)){
                availableDevices.add(device);
            }

            else  {
                System.out.println(" Device " + device+ " is under test and has app installed");
            }

            if (availableDevices.size() == 0){
                throw new RuntimeException("No devices available for testing at the moment");
            }

        }

        return availableDevices;
    }


    public static void createDriver() throws Exception{

        ArrayList<String> devices = getAvailableDevices();
        try {
            for(String device:devices) {
                driver=new AppiumDriver(new AppiumSession().getBuilder(),getCap(device,"browser"));
                ADB adb=new ADB(device);
        }
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
    }


    public static void killDriver(){
        if (driver!=null){
            driver.quit();
        }
    }





}
