package utilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.AndroidServerFlag;
import io.appium.java_client.service.local.flags.IOSServerFlag;
import net.bytebuddy.utility.RandomString;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.Random;

/**
 * Created by vladyslav on 1/15/18.
 */
public class AppiumSession {

    AppiumDriver driver;
   public AppiumDriverLocalService service;
   public AppiumServiceBuilder builder;
  //  int port = 4723;


    public AppiumSession() {
        startAppium();
       // run();
        //this.driver = new AppiumDriver(getBuilder(), capabilities);
    }


    public void startAppium(){

       Integer bootstrapPort = getRandomNumberPortsRange(8000, 9000);
       Integer chromePort = getRandomNumberPortsRange(9001, 9999);
       Integer appiumPort = getRandomNumberPortsRange(4000,5000);

        //Build AppiumService
        builder = new AppiumServiceBuilder();
        builder.withIPAddress("127.0.0.1")
                .usingPort(appiumPort)
                .withArgument(AndroidServerFlag.BOOTSTRAP_PORT_NUMBER,bootstrapPort.toString())
                .withArgument(AndroidServerFlag.CHROME_DRIVER_PORT,chromePort.toString());

        //builder.usingPort(4723);
       // builder.usingAnyFreePort();
      //  builder.withCapabilities(cap);


    }

    public AppiumServiceBuilder getBuilder (){
        return this.builder;
    }


    public void stopAppium (){
        service.stop();


    }

    /*
    @Override
    public void run() {
        startAppium();
    }
    */


    public AppiumDriver driver (){
        return this.driver;
    }

    private int getRandomNumberPortsRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

}
