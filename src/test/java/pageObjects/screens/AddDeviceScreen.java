package pageObjects.screens;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

/**
 * Created by vladyslav on 1/8/18.
 */
public class AddDeviceScreen extends BaseScreen {


    @AndroidFindBy(id = "scan_button")
    public WebElement scanBtn;

    @AndroidFindBy (id = "enter_id_manually_button")
    public  WebElement typeCredentialsBtn;



    public AddDeviceScreen(AppiumDriver driver){

        super(driver);

    }


}
