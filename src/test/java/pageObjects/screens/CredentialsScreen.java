package pageObjects.screens;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;


/**
 * Created by vladyslav on 1/9/18.
 */
public class CredentialsScreen extends BaseScreen {



    @AndroidFindBy (id="enter_device_id")
    public WebElement idField;

    @AndroidFindBy (id="enter_device_secret")
    public WebElement passField;

    @AndroidFindBy (id="action_setup_done")
    public WebElement doneBtn;

    public CredentialsScreen(AppiumDriver driver) {
        super(driver);
    }

    public CredentialsScreen setDeviceId ( String deviceId){

        idField.sendKeys(deviceId);

        return this;
    }

    public CredentialsScreen setDevicePassword ( String password) {
       passField.sendKeys( password);

        return this;
    }

}
