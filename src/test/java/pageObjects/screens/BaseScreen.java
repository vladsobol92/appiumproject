package pageObjects.screens;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

/**
 * Created by vladyslav on 1/9/18.
 */
public class BaseScreen {


  public BaseScreen(AppiumDriver driver){
        initElements(driver);
    }

    public void initElements (AppiumDriver driver ){
        PageFactory.initElements(new AppiumFieldDecorator(driver,60, TimeUnit.SECONDS), this);
    }

}
