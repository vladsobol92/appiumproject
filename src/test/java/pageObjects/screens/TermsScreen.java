package pageObjects.screens;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.annotations.Test;
import pageObjects.UiSelector;


/**
 * Created by vladyslav on 1/9/18.
 */
public class TermsScreen extends BaseScreen {

  // private  String locator="new UiSelector()";

    AppiumDriver driver;
    AndroidDriver driver2;

    @AndroidFindBy(id = "tos_next_button")
    public WebElement nextBtn;

    @AndroidFindBy (id="tos_title")
    @FindBy (tagName = "h1")
    public WebElement title;

    @AndroidFindBy(id="tos_service_link_info")
   public WebElement serviceLink;

    @AndroidFindBy(uiAutomator =   "new UiSelector().text(\"sometext\")")
    WebElement test;

    public TermsScreen(AppiumDriver driver) {

        super(driver);
        this.driver=driver;

    }



    public AddDeviceScreen proceedToApp(){
         nextBtn.click();
        return new AddDeviceScreen(driver);
    }



    public AddDeviceScreen testMethod(String value){

       // locator=locator+".resourceId(\""+value+"\")";

       // driver2.findElementByAndroidUIAutomator()

        driver2.findElementByAndroidUIAutomator("new UiSelector().description(\""+value+"\")");

      //  driver2.findElementByAndroidUIAutomator(locator).getAttribute("clickable");

        driver2.findElementsByAndroidUIAutomator(new UiSelector().description("test").locator);


        By element2 = new MobileBy.ByAndroidUIAutomator("new UiSelector().description(\"+value+\")");

        return new AddDeviceScreen(driver);
    }

    @Test
    public void testLocator(){
        System.out.println("hello");

        System.out.println(
                new UiSelector().description("test").locator
        );
    }
}
