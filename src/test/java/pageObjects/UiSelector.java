package pageObjects;


public class UiSelector {

    public  String locator="new UiSelector()";

    public UiSelector description (String value){
        locator+=".description(\""+value+"\")";
        return this;
    }

    public UiSelector text (String value){
        locator=locator+".text(\""+value+"\")";
        return this;
    }

    public UiObject makeUiObject(){
        return new UiObject(locator);
    }

}

